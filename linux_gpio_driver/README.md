# Chapter 7: LED button-switch device driver & Python web application
The hardware used here consists of an RPi, with one of its GPIO pins connected to an LED, and another GPIO pin connected to a press-button switch. The purpose of the switch is to generate an interrupt to the RPi, which causes the LED to toggle from OFF to ON, or ON to OFF. These drivers demonstrate so-called top-half and bottom-half interrupt handling. The top-half is for quick and dirty work. The bottom-half is for more heavy duty work. I use the same top-half mechanism for both drivers, but demo two different bottom-half methods. I also illustrate the use of spinlocks to protect share resources from simultaneous access by multiple threads on multic-core processors like the RPi.

### led_switch_driver/module_tasklet
### led_switch_driver/module_threaded
The folder _module_tasklet_ contains a driver _led_switch_tasklet.c_ which uses a tasklet for its bottom-half interrupt processing. The folder _module_threaded_ has a driver called _led_switch_threaded.c_ which uses a threaded interrupt handler for b-h processing. Previous chapters focused on driving outputs only. Here we add an asynchronous input (a press-button switch) and show how the Linux driver can manage this safely. There are now two ways to toggle the LED on/off - from a web app, or from the button switch. This introduces possible race conditions. We show how to use spinlock functions to prevent race conditiosn from corrupting critical code.
### led_switch_driver/user
Simple c prpgrom to toggle the LED. After compiling the program with the _Makefile_, enter the following:   
**$** ./demo /dev/led_switch_dev on _or_ off _or_ query
### led_switch_driver/webpy
To start the Flask-based web app enter:
**$** ./controller
In a browser navigate to the IP address of your RPi, for example like so: http://192.168.2.2:5000  
You should see a Driver Test Page.
#### led_switch_driver/webpy/templates
This folder contains the file _led_onff_refresh.html_ for displaying the Driver Test Page. It is similar to the html file used in Chapter-06, however I've added the following line of code:  
`meta http-equiv="refresh" content="1"`  
This refreshes the page every one second so that it catches and displays any LED on/off change caused by pressing the button switch.


