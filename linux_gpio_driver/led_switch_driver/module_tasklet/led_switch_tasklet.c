/* 
 * led_switch_tasklet.c
 * switch controlled LED driver with tasklet bottom half interrupt processing 
 */

#include "led_switch.h"
#define MODULE_NAME "led_switch_tasklet"

// led device struct
typedef struct led_switch_struct {
    int ledPin;
	int buttonPin;
    bool on;             //LED status
    bool busy;           //stops more than one user accessing LED pin
    u64 debounce;        //switch debounce delay
	int irq;
	bool triggered;
	struct miscdevice mdev;
    spinlock_t my_spinlock; //access to some parameters must be protected by a spinlock
	struct tasklet_struct my_tasklet;
} led_switch_struct_t;
static led_switch_struct_t pi_led = {0};

// methods
static const struct file_operations led_fops = {
    .owner = THIS_MODULE,
    .open = led_open,
    .release = led_release,
    .read = led_read,
    .write = led_write,
};

// iniatilize and register device
static int setup_mydevice(led_switch_struct_t *dev) 
{
    dev->mdev.minor = MISC_DYNAMIC_MINOR;
    dev->mdev.name = DEVICE_NAME;
    dev->mdev.fops = &led_fops;
	dev->mdev.mode = 0666;
    return misc_register(&dev->mdev);	
}

// configure GPIO pins
static int configure_gpio(led_switch_struct_t *dev) 
{
    dev->ledPin = led_gpio;
	dev->buttonPin = button_gpio;
	dev->irq = gpio_irqnum(dev->buttonPin);
	PDEBUG("Linux IRQ Number = %d\n", dev->irq);
	if (gpio_map() < 0) return -ENOMEM;
    gpio_set_mode(dev->ledPin, GPIO_OUTPUT);
    // if your gpio defaults to an input at boot-up, line below not needed. 
	gpio_set_mode(dev->buttonPin, GPIO_INPUT);
	return 0;
}

// unmap GPIO
static void unconfigure_gpio(led_switch_struct_t *dev)
{
    dev->on = false;
    gpio_set_state(dev->ledPin, dev->on);
    gpio_unmap();
}

// led control routines
static bool get_led_state(led_switch_struct_t *dev)
{
    bool ret;
    unsigned long flags;
	
    spin_lock_irqsave(&dev->my_spinlock, flags);
    ret = dev->on;
    spin_unlock_irqrestore(&dev->my_spinlock, flags);
    return ret;
}

static void set_led_state(led_switch_struct_t *dev) 
{
	unsigned long flags;
	
	spin_lock_irqsave(&dev->my_spinlock, flags);
	gpio_set_state(dev->ledPin, dev->on);
	spin_unlock_irqrestore(&dev->my_spinlock, flags);
}

static void toggle_led(led_switch_struct_t *dev)
{
	if (dev->on == false) {
		PDEBUG("Toggling led on\n");
		dev->on = true;
	} 
	else if (dev->on == true) {
		PDEBUG("Toggling led off\n");
		dev->on = false;
	}
	gpio_set_state(dev->ledPin, dev->on);
}

// interrupt handling routines
static irqreturn_t top_half_isr(int irq , void *dev_id) 
{
	led_switch_struct_t *dev = dev_id;
	
	PCONTEXT();
	tasklet_schedule(&dev->my_tasklet); //set tasklet
	gpio_ack_interrupts();	//acknowledge interrupts by writing FFF.. 
	//gpio_disable_interrupts();	//turn off the GPIO interrupts pending the bottom half completing
	return IRQ_HANDLED;
}

static void bottom_half_tasklet(unsigned long data)
{
    unsigned long flags;
    bool is_pressed;
	led_switch_struct_t *dev = (led_switch_struct_t *) data;
	
	spin_lock_irqsave(&dev->my_spinlock, flags);
	is_pressed = button_pressed(dev->buttonPin);
	// software debounce of switch press
    if (is_pressed && get_jiffies_64() > dev->debounce) {
		PCONTEXT();
        dev->debounce = get_jiffies_64() + DEBOUNCE_DELAY;
        toggle_led(dev); 
    }
    spin_unlock_irqrestore(&dev->my_spinlock, flags);
	//gpio_enable_interrupt(dev->buttonPin);
}

// initialise driver and GPIO when module is loaded
static __init int led_init(void)
{
    int err = 0;
	led_switch_struct_t *dev = &pi_led;
	
    // initialise GPIO registers and pins
	err = configure_gpio(dev);
	if (err) goto err_gpio_map;
    
	// register device with kernel
	err = setup_mydevice(dev);
	if(err)	goto err_misc_reg;
	pr_info("Loaded module: %s\n", MODULE_NAME);
    dev_info(dev->mdev.this_device, "MAJOR = %i, MINOR = %i\n", 
		MISC_MAJOR, dev->mdev.minor);
    
	// configure interrupt context
	spin_lock_init(&dev->my_spinlock);
	tasklet_init(&dev->my_tasklet, bottom_half_tasklet, (unsigned long)dev);
	err = devm_request_irq(dev->mdev.this_device, dev->irq, top_half_isr, 0, MODULE_NAME, dev);
	if (err) goto err_request_irq; 
	gpio_enable_interrupt(dev->buttonPin);
	
    dev->on = true;
	set_led_state(dev);
	return 0;

err_gpio_map:
	pr_alert("Memory Error!\n");
	return -ENOMEM;
err_misc_reg:
    dev_err(dev->mdev.this_device, "Could not register misc device\n");
    unconfigure_gpio(dev);
	return -ENODEV;
err_request_irq:
    dev_err(dev->mdev.this_device, "Could not assign interrupt handler to irq number\n");
    unconfigure_gpio(dev);
	misc_deregister(&dev->mdev);
	return -ENODEV;
}

// clean up when module is unloaded
static void cleanup(led_switch_struct_t *dev)
{
	tasklet_kill(&dev->my_tasklet);
	unconfigure_gpio(dev);
    misc_deregister(&dev->mdev);    
    pr_info("Ending module: " MODULE_NAME "\n");
}

static __exit void led_exit(void)
{
	led_switch_struct_t *dev = &pi_led;
	cleanup(dev);
}

static inline led_switch_struct_t *to_led_switch_struct(struct file *filep)
{    
	struct miscdevice *miscdev = filep->private_data;
    return container_of(miscdev, led_switch_struct_t, mdev);
}

// driver methods
static int led_open(struct inode *inodep, struct file *filep)
{
	int ret = 0;
	unsigned long flags;	
	led_switch_struct_t *dev;

	dev = to_led_switch_struct(filep); 
	filep->private_data = dev;
	
	PCONTEXT();
    PDEBUG("Opening %s\n", DEVICE_NAME);
	spin_lock_irqsave(&dev->my_spinlock, flags); //ensures that two threads do not access at the same time
    if (dev->busy)
        ret = -EBUSY;
    else
		dev->busy = true;
    spin_unlock_irqrestore(&dev->my_spinlock, flags);
    return ret;
}

static int led_release(struct inode *inode, struct file *filep)
{
	unsigned long flags;
	led_switch_struct_t *dev;
	
	dev = filep->private_data;
	PCONTEXT();
    PDEBUG("Closing %s\n", DEVICE_NAME);
    spin_lock_irqsave(&dev->my_spinlock, flags);
	dev->busy = false;
    spin_unlock_irqrestore(&dev->my_spinlock, flags);
    return 0;
}

static ssize_t led_read(struct file *filep, char __user *user_buf,
                  size_t count, loff_t *offs)
{
    char *led_buf;
    int err;
	led_switch_struct_t *dev;
	
	//PCONTEXT();
	dev = filep->private_data;
	led_buf = get_led_state(dev) ? "1" : "0";
    count = sizeof(*led_buf);
    err = copy_to_user(user_buf, led_buf, count);
    if (err) return -EFAULT;
    return count;
}

#define LEN 2
static ssize_t led_write(struct file *filep, const char __user *user_buf,
                  size_t count, loff_t *offs)
{
    int err;
    char led_buf[LEN];
	led_switch_struct_t *dev;
	
	PCONTEXT();
	dev = filep->private_data;
    err = copy_from_user(led_buf, user_buf, LEN);
    if (err) return -EFAULT;

    if (led_buf[0] == '0') {
        PDEBUG("Turning led off\n");
        dev->on = false;
    } else if (led_buf[0] == '1') {
        PDEBUG("Turning led on\n");
        dev->on = true;
    }
	set_led_state(dev);
    return LEN;
}

module_init(led_init);
module_exit(led_exit);
MODULE_LICENSE("GPL");
