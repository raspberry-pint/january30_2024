#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int turn_on(int fd)
{
    static const char buf[] = "1";
    return write(fd, buf, sizeof(buf));
}

int turn_off(int fd)
{
    static const char buf[] = "0";
    return write(fd, buf, sizeof(buf));
}

int query(int fd)
{
    char buf[2];
    int ret = read(fd, buf, sizeof(buf));

    if (ret < 1) return ret;
    switch (buf[0]) {
    case '1':
        printf("Led is ON\n");
        return 0;
    case '0':
        printf("Led is OFF\n");
        return 0;
    default:
        fprintf(stderr, "Unexpected value\n");
        return 0;
    }
}

static const struct {
    const char* name;
    int (*func)(int fd);
} ops[] = {
    {
        .name = "on",
        .func = turn_on,
    },
    {
        .name = "off",
        .func = turn_off,
    },
    {
        .name = "query",
        .func = query,
    },
};

int main(int argc, char**argv)
{
    int fd = -1;
    int i;
    int error;
    static const size_t kops_count = sizeof(ops) / sizeof(ops[0]);
    if (argc < 3) {
        fprintf(stderr, "Usage: ./demo <device> on|off|query\n");
        return EXIT_FAILURE;
    }
    fd = open(argv[1], O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "Error opening file %s : %s\n", argv[1], strerror(errno));
        return EXIT_FAILURE;
    }

    for (i = 0; i < kops_count; ++i) {
        if (strcmp(ops[i].name, argv[2]) == 0) {
            error = ops[i].func(fd);
            break;
        }
    }
    if (i == kops_count) {
        fprintf(stderr, "Unexpected op %s\n", argv[2]);
    }
    close(fd);
    return error;
}
