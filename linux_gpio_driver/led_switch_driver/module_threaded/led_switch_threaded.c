/* led_switch_threaded.c : switch controlled LED driver */

#include "led_switch.h"
#define MODULE_NAME "led_switch_threaded"
#define USE_KERNEL_HARDIRQ false;
static bool kernel_irq_handler = USE_KERNEL_HARDIRQ; 
module_param(kernel_irq_handler, bool, 0444);

// forward declarations
static irqreturn_t my_primary_irq_handler(int irq , void *dev_id);
static irqreturn_t irq_thread_handler(int irq , void *dev_id);

// led device struct
typedef struct led_switch_struct {
    int ledPin;
	int buttonPin;
    bool on; 
    bool busy; 
    u64 debounce;
	int irq;
	bool triggered;
    spinlock_t spinlock;
	struct mutex lock;	
    struct miscdevice mdev;
} led_switch_struct_t;
static led_switch_struct_t pi_led = {0};

// methods
static const struct file_operations led_fops = {
    .owner = THIS_MODULE,
    .open = led_open,
    .release = led_release,
    .read = led_read,
    .write = led_write,
};

// iniatilize and register device
static int setup_mydevice(led_switch_struct_t *dev) 
{
    dev->mdev.minor = MISC_DYNAMIC_MINOR;
    dev->mdev.name = DEVICE_NAME;
    dev->mdev.fops = &led_fops;
	dev->mdev.mode = 0666;
    return misc_register(&dev->mdev);	
}

// configure GPIO pins
static int configure_gpio(led_switch_struct_t *dev) 
{
    dev->ledPin = led_gpio;
	dev->buttonPin = button_gpio;
	dev->irq = gpio_irqnum(dev->buttonPin);
	PDEBUG("Linux IRQ Number = %d\n", dev->irq);
	if (gpio_map() < 0) return -ENOMEM;
    gpio_set_mode(dev->ledPin, GPIO_OUTPUT);
    // if your gpio defaults to an input at boot-up, line below not needed. 
	gpio_set_mode(dev->buttonPin, GPIO_INPUT);
	return 0;
}

// unmap GPIO
static void unconfigure_gpio(led_switch_struct_t *dev)
{
    dev->on = false;
    gpio_set_state(dev->ledPin, dev->on);
    gpio_unmap();
}

// led control routines
static bool get_led_state(led_switch_struct_t *dev)
{
    bool ret;
    unsigned long flags;
	
    spin_lock_irqsave(&dev->spinlock, flags);
    ret = dev->on;
    spin_unlock_irqrestore(&dev->spinlock, flags);
    return ret;
}

static void set_led_state(led_switch_struct_t *dev) 
{
	unsigned long flags;
	
	spin_lock_irqsave(&dev->spinlock, flags);
	gpio_set_state(dev->ledPin, dev->on);
	spin_unlock_irqrestore(&dev->spinlock, flags);
}

static void toggle_led(led_switch_struct_t *dev)
{
	if (dev->on == false) {
		PDEBUG("Toggling led on\n");
		dev->on = true;
	} 
	else if (dev->on == true) {
		PDEBUG("Toggling led off\n");
		dev->on = false;
	}
	gpio_set_state(dev->ledPin, dev->on);
}

static int register_interrupt_handlers(led_switch_struct_t *dev)
{
    int err = 0;
	irq_handler_t primary_handler;
	
	if (kernel_irq_handler) {
		primary_handler = NULL;
		PDEBUG("Using kernel's default primary irq handler\n");
	}
	else {
		primary_handler = my_primary_irq_handler;
		PDEBUG("Using my primary irq handler\n");
	}
	err = devm_request_threaded_irq(dev->mdev.this_device, dev->irq, primary_handler, irq_thread_handler, IRQF_ONESHOT, MODULE_NAME, dev);
    /*
     * IRQF_ONESHOT - Interrupt is not reenabled after primary irq handler finished.
     *                Used by threaded interrupts which need to keep the
     *                irq line disabled until the threaded handler has been run.
     *                Allows drivers to request that the interrupt is not unmasked after
     *                the hard interrupt context handler has been executed and the thread 
     *                has been woken. The interrupt line is unmasked after the thread handler
     *                function has been executed.
     */
    if (err) {
        dev_err(dev->mdev.this_device, "Could not assign interrupt handler to irq number\n");
        return err;
    }
	gpio_enable_interrupt(dev->buttonPin);
    return 0;
}

// top half interrupt handler
static irqreturn_t my_primary_irq_handler(int irq , void *dev_id) 
{
	led_switch_struct_t *dev = dev_id;
	
	dev->triggered = get_gpio_trigger(dev->buttonPin);
	if (!dev->triggered)
		return IRQ_NONE;
	/* the above check that it was our button that triigered
	the interrupt is only really necessary when using shared
	interrupts */
	
	PCONTEXT();
	gpio_ack_interrupts();	//acknowledge interrupts by writing FFF.. 
    return IRQ_WAKE_THREAD;
}

// bottom half interrupt handler
static irqreturn_t irq_thread_handler(int irq , void *dev_id)
{
    bool is_pressed;
	led_switch_struct_t *dev = dev_id;
	
	if (kernel_irq_handler)
		gpio_ack_interrupts();
	mutex_lock(&dev->lock);
	is_pressed = button_pressed(dev->buttonPin);
	// software debounce of switch press
    if (is_pressed && get_jiffies_64() > dev->debounce) {
		PCONTEXT();
        dev->debounce = get_jiffies_64() + DEBOUNCE_DELAY;
        toggle_led(dev); 
    }
    mutex_unlock(&dev->lock);
	return IRQ_HANDLED;
}

// initialise driver and GPIO when module is loaded
static __init int led_init(void)
{
    int err = 0;

    // initialise GPIO registers and pins
	err = configure_gpio(&pi_led);
	if (err) goto err_gpio_map;
    
	// register device with kernel
	err = setup_mydevice(&pi_led);
	if(err)	goto err_misc_reg;
	pr_info("Loaded module: %s\n", MODULE_NAME);
    dev_info(pi_led.mdev.this_device, "MAJOR = %i, MINOR = %i\n", 
		MISC_MAJOR, pi_led.mdev.minor);
    
	// configure interrupt context
	spin_lock_init(&pi_led.spinlock);
	err = register_interrupt_handlers(&pi_led);
	if (err) goto err_request_irq; 
	
    pi_led.on = true;
	set_led_state(&pi_led);
	
	return 0;

err_gpio_map:
	pr_alert("Memory Error!\n");
	return -ENOMEM;
err_misc_reg:
    dev_err(pi_led.mdev.this_device, "Could not register misc device\n");
    unconfigure_gpio(&pi_led);
	return -ENODEV;
err_request_irq:
    unconfigure_gpio(&pi_led);
	misc_deregister(&pi_led.mdev);
	return -ENODEV;
}

// clean up when module is unloaded
static void cleanup(led_switch_struct_t *dev)
{
	unconfigure_gpio(dev);
    misc_deregister(&dev->mdev);    
    pr_info("Ending module: " MODULE_NAME "\n");
}

static __exit void led_exit(void)
{
	cleanup(&pi_led);
}

static inline led_switch_struct_t *to_led_switch_struct(struct file *filep)
{    
	struct miscdevice *miscdev = filep->private_data;
    return container_of(miscdev, led_switch_struct_t, mdev);
}

// driver methods
static int led_open(struct inode *inodep, struct file *filep)
{
	int ret = 0;
	unsigned long flags;	
	led_switch_struct_t *dev;

	dev = to_led_switch_struct(filep); 
	filep->private_data = dev;
	
    PCONTEXT();
	PDEBUG("Opening %s\n", DEVICE_NAME);
	spin_lock_irqsave(&dev->spinlock, flags); //ensures that two threads do not access at the same time
    if (dev->busy)
        ret = -EBUSY;
    else
		dev->busy = true;
    spin_unlock_irqrestore(&dev->spinlock, flags);
    return ret;
}

static int led_release(struct inode *inode, struct file *filep)
{
	unsigned long flags;
	led_switch_struct_t *dev;
	
	dev = filep->private_data;
	PCONTEXT();
    PDEBUG("Closing %s\n", DEVICE_NAME);
    spin_lock_irqsave(&dev->spinlock, flags);
	dev->busy = false;
    spin_unlock_irqrestore(&dev->spinlock, flags);
    return 0;
}

static ssize_t led_read(struct file *filep, char __user *user_buf,
                  size_t count, loff_t *offs)
{
    char *led_buf;
    int err;
	led_switch_struct_t *dev;
	
	dev = filep->private_data;
	led_buf = get_led_state(dev) ? "1" : "0";
    count = sizeof(*led_buf);
    err = copy_to_user(user_buf, led_buf, count);
    if (err) return -EFAULT;
    return count;
}

#define LEN 2
static ssize_t led_write(struct file *filep, const char __user *user_buf,
                  size_t count, loff_t *offs)
{
    int err;
    char led_buf[LEN];
	led_switch_struct_t *dev;
	
	PCONTEXT();
	dev = filep->private_data;
    err = copy_from_user(led_buf, user_buf, LEN);
    if (err) return -EFAULT;

    if (led_buf[0] == '0') {
        PDEBUG("Turning led off\n");
        dev->on = false;
    } else if (led_buf[0] == '1') {
        PDEBUG("Turning led on\n");
        dev->on = true;
    }
	set_led_state(dev);
    return LEN;
}

module_init(led_init);
module_exit(led_exit);
MODULE_LICENSE("GPL");
