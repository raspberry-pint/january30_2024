#!/usr/bin/python3
from flask import Flask
from flask import request
from flask import Response
from flask import render_template
from flask import make_response

import led_onoff

k_action = '/index.py'
k_btnTurnOn = 'btnTurnOn'
k_btnTurnOff = 'btnTurnOff'

app = Flask(__name__)

class BadPost(Exception):
    pass

def process_form():
    if k_btnTurnOn in request.form:
        print("Turning LED on")
        led_onoff.turn_on()
    elif  k_btnTurnOff in request.form:
        print("Turning LED off")
        led_onoff.turn_off()
    else:
        raise BadPost
        
@app.route('/', methods = ['GET', 'POST'])
@app.route('/index.py', methods = ['GET', 'POST'])
def index():
    try:
        if request.method == 'POST':
            process_form()
            
    except BadPost:
        response = make_response('Bad Post')
        response.status_code = 403
        return response

    print ("got here3")
    state = led_onoff.query()
    print("state = ", state)
    return render_template('led_onff_refresh.html',
            action=k_action,
            led_state=state,
            btnTurnOn=k_btnTurnOn,
            btnTurnOff=k_btnTurnOff
            )
            
if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')

